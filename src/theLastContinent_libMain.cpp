#ifdef openglGameTemplate_libMain_cpp
#error Multiple inclusion
#endif
#define openglGameTemplate_libMain_cpp

#include "common.cpp"
#include "allocator.hpp"
#include "HeapAllocator.cpp"
#include "PageAllocator.cpp"
#include "Str.cpp"
#include "StrBuf.cpp"
#include "print.cpp"
#include "vector.hpp"
#include "matrix.hpp"
#include "print_vector.hpp"
#include "Grid.hpp"
#include "args.cpp"
#include "ScopeExit.hpp"
#include "List.hpp"
#include "Prng.cpp"

#include "gl3w_Functions.cpp"
#include "openglGame_common.cpp"

#include "assetManager_config_assetType.hpp"
#include "openglGame_AssetLabel.hpp"

#include "magicaVoxel_Mesh.hpp"
#include "shaders_glsl.cpp"
#include "ImGui_Adaptor.hpp"

#include "Assets.cpp"

#include "hex.cpp"
#include "theLastContinent.cpp"

#include "openglGame_exports.cpp"
